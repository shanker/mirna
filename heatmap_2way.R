#This scripts performed rowwise (miRNA) and columnwise (time-points) cluster analysis and plots integrated heatmap
setwd(file.path("home","srao2","Documents","KUMC","physiol_kumc","mirna")) #setting up working directory
times <- read.table("RS08_T00A", sep="\t", header=TRUE)             #reading time-series datafile, RS08_T00A
filter_rowcol <- times[times$Species_Scientific_Name=="Homo sapiens" & (times$Sequence_Type == "miRNA" | times$Sequence_Type == "stem-loop"), c('ID_REF', 'X2_mons_prostate', 'X4_mons_prostate', 'X6monsP_0823', 'X8monsP_0823', 'X10monsP_0823')]
                      #filter rows and columns as per criteria defined
filter_rowcol$var <- apply(filter_rowcol[, 2:6], 1, var)            #compute variance for each miRNA across 5 time-points
filter_var <- filter_rowcol[filter_rowcol$var > 1.5, ]              #remove miRNAs with variance < 1.5
row.names(filter_var) <- filter_var$ID_REF                          #row-label for cluster-heatmap
d <- data.matrix(filter_var[,2:6])
distance_r <- dist(d)                                               #distance matrix
hclust_complete_r <- hclust(distance_r, method = "complete")        #hierarchical clustering for miRNAs
dendrow <- as.dendrogram(hclust_complete_r)                         #dendrogram for miRNAs
distance_c <- dist(t(d))                                            #distance matrix
hclust_complete_c <- hclust(distance_c, method = "complete")        #hierarchical clustering for time-points
dendcol <- as.dendrogram(hclust_complete_c)                         #dendrogram of time-points
heatmap(d, Rowv=dendrow, Colv=dendcol, col = heat.colors(256), cexCol=0.7, scale="column", margins=c(6,5))
                        #heatmap summarizing 2-way clustering