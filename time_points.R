#This script generates expression vs time-point plot
setwd(file.path("home","srao2","Documents","KUMC","physiol_kumc","mirna")) #setting up working directory
times <- read.table("RS08_T00A", sep="\t", header=TRUE)             #reading time-series datafile, RS08_T00A
filter_rowcol <- times[times$Species_Scientific_Name=="Homo sapiens" & (times$Sequence_Type == "miRNA" | times$Sequence_Type == "stem-loop"), c('ID_REF', 'X2_mons_prostate', 'X4_mons_prostate', 'X6monsP_0823', 'X8monsP_0823', 'X10monsP_0823')]
                      #filter rows and columns as per criteria defined
filter_rowcol$var <- apply(filter_rowcol[, 2:6], 1, var)            #compute variance for each miRNA across 5 time-points
filter_var <- filter_rowcol[filter_rowcol$var > 1.5, ]              #remove miRNAs with variance < 1.5
#plot 322 miRNA expressions on same plot
for(i in 1:322) {
  x  <- t(filter_var[i, 2:6])
  plot(x, main="Time-series, 322 miRNA", xlab="Time-points -->", ylab="miRNA expression -->", type="l", col=2, axes=TRUE)
  par(ann=FALSE, xaxt="n", yaxt="n", new=TRUE)
}